#include "CKula.h"
#include <cstdlib>
#include <ctime>

CKula::CKula(float p) : promien(p)
{}

CKula::CKula(float i, const WektorXY& p, const WektorXY& k):
    promien(i)
{wspolrzedne=p;
predkosc = k;}

WektorXY& CKula::pobierz_wymiary()
{
    wymiary.ustaw_wspolrzedne(std::pair<float, float> {promien, -1});
    return wymiary;
}

void CKula::ustaw_predkosc(const WektorXY& p)
{predkosc = p;}

void CKula::ustaw_wspolrzedne(const WektorXY& p)
{wspolrzedne = p;}
