#include "WektorXY.h"
#include <cmath>
#include <iostream>

const double PI = 3.14159265359;

WektorXY::WektorXY(std::pair<float, float> p) : wspolrzedne(p)
{
    dlugosc = sqrt(pow(p.first, 2) + pow(p.second, 2));
    fi = asin(p.second/dlugosc);
}

WektorXY::WektorXY(float d, float f) : dlugosc(d), fi(f)
{
    float kx = cos(f)*d;
    float ky = sin(f)*d;
    wspolrzedne = {kx, ky};
}

WektorXY::~WektorXY()
{}

void WektorXY::ustaw_fi(const float& f)
{
    if (f < 2*PI)
        fi = f;
    else{
        int c = floor(f/(2*PI));
        fi = f - c*2*PI;
    }
    wspolrzedne.first = dlugosc*cos(fi);
    wspolrzedne.second = dlugosc*sin(fi);
}

void WektorXY::ustaw_dlugosc(const float& d)
{
    dlugosc = d;
    wspolrzedne.first = d*cos(fi);
    wspolrzedne.second = d*sin(fi);
}

void WektorXY::ustaw_wspolrzedne(const std::pair<float, float>& p)
{
    wspolrzedne.first = p.first;
    wspolrzedne.second = p.second;
    dlugosc = sqrt(pow(p.first, 2) + pow(p.second, 2));
    fi = asin(p.second/dlugosc);
}

std::pair<float, float> WektorXY::pobierz_wspolrzedne() const
{
    return wspolrzedne;
}

float WektorXY::pobierz_fi() const
{
    return fi;
}

WektorXY WektorXY::operator+(const WektorXY& W) const // pobieramy 'tylko' referencj�, kt�rej nie modyfikujemy
{
    std::pair<float, float> p = {wspolrzedne.first + W.pobierz_wspolrzedne().first, wspolrzedne.second + W.pobierz_wspolrzedne().second};
    return WektorXY(p);
}

WektorXY WektorXY::operator- ()
{
    wspolrzedne = {-wspolrzedne.first, -wspolrzedne.second};
    fi = fi + PI;
    return *this;
}

WektorXY WektorXY::operator+ () const
{
    return *this;
}

WektorXY WektorXY::operator- (const WektorXY& W) const
{
    std::pair<float, float> p = {wspolrzedne.first - W.pobierz_wspolrzedne().first, wspolrzedne.second - W.pobierz_wspolrzedne().second};
    return WektorXY(p);
}

WektorXY& WektorXY::orto()
{
    float p = pobierz_wspolrzedne().first;
    float d = pobierz_wspolrzedne().second;
    p = -p;
    float pom = p;
    p = d;
    d = pom;
    ustaw_wspolrzedne(std::pair<float, float>{p, d});
    return *this;
}

void WektorXY::oblicz_kat(const WektorXY& dana)
{
    float kat_d = dana.pobierz_fi();
    float kat = pobierz_fi();
    if (kat_d < 0)
        kat_d = PI + kat_d;
    float wynik = kat - kat_d;

    if (wynik < 0)
        ustaw_fi(this->pobierz_fi() + 2*wynik);
    else
        ustaw_fi(this->pobierz_fi() - 2*wynik);
}
