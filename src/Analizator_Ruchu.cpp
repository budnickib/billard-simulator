#include "Analizator_Ruchu.h"
#include "CKula.h"

Analizator_Ruchu::Analizator_Ruchu(std::vector<CObiekt*>& tablica) : tab(tablica)
{}

Analizator_Ruchu::~Analizator_Ruchu()
{
    //dtor
}

bool Analizator_Ruchu::sprawdz_obszar(CObiekt* const spr)
{
    WektorXY wym2 = spr->pobierz_wymiary();
    WektorXY pol2 = spr->pobierz_wspolrzedne();
    float xp, yp, a, b, p, xk, yk;
    WektorXY pom;

    for(std::vector<CObiekt*>::const_iterator i=tab.begin(); i !=tab.end(); ++i)
    {
        if (spr==(*i)) continue;
        WektorXY wym1 = (*i)->pobierz_wymiary();
        WektorXY pol1 = (*i)->pobierz_wspolrzedne();

        if(wym1.pobierz_wspolrzedne().second == -1) //kula
        {
            if(wym2.pobierz_wspolrzedne().second == -1) //kula
            {
                pom = pol1 - pol2;
                if (pom.pobierz_dlugosc() < wym1.pobierz_wspolrzedne().first + wym2.pobierz_wspolrzedne().first)
                    return false;
            }else   //prostokat i kula, wym1 = kula, wym2 = prostokat
            {
                xp = pol2.pobierz_wspolrzedne().first;
                yp = pol2.pobierz_wspolrzedne().second;
                a = wym2.pobierz_wspolrzedne().first;
                b = wym2.pobierz_wspolrzedne().second;
                p = wym1.pobierz_wspolrzedne().first;
                xk = pol1.pobierz_wspolrzedne().first;
                yk = pol1.pobierz_wspolrzedne().second;

                if (yp+p > yk && yp-a-p < yk && xp-p < xk && xp+b+p>xk)
                    return false;
            }
        }else   //prostokat
        {
            if(wym2.pobierz_wspolrzedne().second == -1) //kula
            {
                xp = pol1.pobierz_wspolrzedne().first;
                yp = pol1.pobierz_wspolrzedne().second;
                a = wym1.pobierz_wspolrzedne().first;
                b = wym1.pobierz_wspolrzedne().second;
                p = wym2.pobierz_wspolrzedne().first;
                xk = pol2.pobierz_wspolrzedne().first;
                yk = pol2.pobierz_wspolrzedne().second;
                if (yp+p > yk && yp-a-p < yk && xp-p < xk && xp+b+p>xk)
                    return false;
            }
        }
    }
    return true;
}


bool Analizator_Ruchu::sprawdz_kolizje(CObiekt* const ob1, CObiekt* const ob2)
{
    if (ob1 == ob2) return false;
    WektorXY wsp1 = ob1->pobierz_wspolrzedne();
    WektorXY wsp2 = ob2->pobierz_wspolrzedne();
//std::cout<<"sprawrz_kolizje"<<std::endl;
    float ob1_x = wsp1.pobierz_wspolrzedne().first;
    float ob1_y = wsp1.pobierz_wspolrzedne().second;
    float ob2_x = wsp2.pobierz_wspolrzedne().first;
    float ob2_y = wsp2.pobierz_wspolrzedne().second;

    float wym1_x = ob1->pobierz_wymiary().pobierz_wspolrzedne().first;
    float wym1_y = ob1->pobierz_wymiary().pobierz_wspolrzedne().second;
    float wym2_x = ob2->pobierz_wymiary().pobierz_wspolrzedne().first;
    float wym2_y = ob2->pobierz_wymiary().pobierz_wspolrzedne().second;

    if ((dynamic_cast<CKula*>(ob1)) != nullptr) //kolo
    {
        if (dynamic_cast<CKula*>(ob2) != nullptr) //kolo, kolo
        {
            WektorXY pomocniczy = wsp1-wsp2;
            float pomd = pomocniczy.pobierz_dlugosc();
            if (pomd>wym1_x+wym2_x) //jezeli sa w kolizji zwroc true
                return false;
            else
            {
                std::cout<<"kolo, kolo"<<std::endl;
                return true;
            }
        }else //kolo - ob1, banda - ob2
        {
            if (ob1_x > ob2_x - wym1_x && ob1_x < ob2_x + wym2_x + wym1_x
                && ob1_y > ob2_y - wym1_x && ob1_y < ob2_y + wym2_y + wym1_x)
            {
                std::cout<<"kolo, banda"<<std::endl;
                return true;
            } else
            {
                return false;
            }
        }
    }else{            //banda
        if(dynamic_cast<CKula*>(ob2) != nullptr) //banda, kolo
        {
            if (ob2_x > ob1_x - wym2_x && ob2_x < ob1_x + wym1_x + wym2_x
                && ob2_y > ob1_y - wym2_x && ob2_y < ob1_y + wym1_y + wym2_x)
            {
                std::cout<<"banda, kolo"<<std::endl;
                return true;
            } else
            {
                std::cout<<"banda, kolo"<<std::endl;
                return false;
            }
        }else{ //banda, banda, przypadek niemozliwy
            return false;
        }
    }
}
