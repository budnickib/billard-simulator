#include "Grafika.h"
#include <iostream>

Grafika::Grafika(CStol* s)
{
    obiekt_obserwowany = s;
    okno.create(sf::VideoMode(1000, 600), "Bilard");
    if(!tekstura_stolu.loadFromFile("bilard.jpg"))
        std::cout<<"blad wczytania tekstury"<<std::endl;

    tlo.setTexture(tekstura_stolu);
    okno.draw(tlo);
}

Grafika::~Grafika()
{
    //dtor
}

void Grafika::update()
{
    okno.clear();
    okno.draw(tlo);
    float wsp_x;
    float wsp_y;
    std::vector<CObiekt*> tablica = obiekt_obserwowany->pobierz_obiekty();
    sf::CircleShape kolo;
    sf::RectangleShape banda;
    WektorXY wsp, pred;
    for (std::vector<CObiekt*>::iterator i = tablica.begin(); i!=tablica.end(); ++i)
    {
        wsp = (*i)->pobierz_wspolrzedne();
        std::pair<float, float> wymiar = (*i)->pobierz_wymiary().pobierz_wspolrzedne();
        float pierwszy = wymiar.first;
        float drugi = wymiar.second;
        wsp_x = wsp.pobierz_wspolrzedne().first;
        wsp_y = wsp.pobierz_wspolrzedne().second;


        pred = (*i)->pobierz_predkosc();
        if (drugi == -1)
        {
            kolo.setRadius(static_cast<int>(pierwszy));
            kolo.setFillColor(sf::Color::Black);
            kolo.setPosition(wsp_x, wsp_y);
            kolo.setOrigin(pierwszy, pierwszy);
            //kolo.move(pred.pobierz_wspolrzedne().first, pred.pobierz_wspolrzedne().second);
            okno.draw(kolo);
        }else
        {
            if (pierwszy == 0)
                pierwszy = 5;
            else if (drugi == 0)
                drugi = 5;
            banda.setSize(sf::Vector2f(pierwszy, drugi));
            banda.setPosition(wsp_x, wsp_y);
            okno.draw(banda);
        }
    }
    okno.display();
}
