#include "CStol.h"
#include "CBanda.h"
#include "CKula.h"

const float PI = 3.141592653;

CStol::CStol(int d, int s):dlugosc(d), szerokosc(s)
{
    analiza = new Analizator_Ruchu(obiekty);

}

CStol::~CStol()
{
    std::vector<CObiekt*>::iterator it;
    for(it = obiekty.begin(); it != obiekty.end(); ++it)
        delete *it;
    delete analiza;
}

void CStol::dodaj_kule(float q, const WektorXY& w, const WektorXY& e)
{

    CObiekt* nowa_kula = new CKula(q, w, e);
    if(analiza ->sprawdz_obszar(nowa_kula))
        obiekty.push_back(nowa_kula);
    else delete nowa_kula;
}

void CStol::dodaj_bande(const WektorXY& w, const WektorXY& q)
{
    CObiekt* nowa_banda = new CBanda(w, q);
    if (analiza ->sprawdz_obszar(nowa_banda))
        obiekty.push_back(nowa_banda);
    else delete nowa_banda;
}

void CStol::dodaj_obserwatora(Obserwator* o)
{
    obserwatorzy.push_back(o);
}

void CStol::usun_obserwatora(Obserwator* o)
{

}

void CStol::powiadamiaj_obserwatorow()
{
    for (std::vector<Obserwator*>::iterator i = obserwatorzy.begin(); i!=obserwatorzy.end(); ++i)
        (*i)->update();
}

void CStol::poruszaj()
{
    for(std::vector<CObiekt*>::iterator i = obiekty.begin(); i!=obiekty.end(); ++i)
        for(std::vector<CObiekt*>::iterator j = i; j!=obiekty.end(); ++j){
            if (analiza->sprawdz_kolizje((*i), (*j)))
            {
                if (dynamic_cast<CKula*>((*i)) && dynamic_cast<CKula*>((*j)))
                {
                    WektorXY pom_i = (*i)->pobierz_wspolrzedne();
                    WektorXY pom_j = (*j)->pobierz_wspolrzedne();
                    WektorXY roznica = pom_i-pom_j;
                    WektorXY pred_i = (*i)->pobierz_predkosc();
                    WektorXY pred_j = (*j)->pobierz_predkosc();
                    pred_i.oblicz_kat(roznica.orto());
                    pred_j.oblicz_kat(roznica.orto());
                    (*i)->ustaw_predkosc(pred_i);
                    (*j)->ustaw_predkosc(pred_j);
                    std::cout<<"ustawiam predkosc kul"<<std::endl;
                }else if (dynamic_cast<CKula*>((*i)) && dynamic_cast<CBanda*>((*j)))
                {
                    WektorXY predkosc_k = (*i)->pobierz_predkosc();
                    WektorXY wsp_k = (*i)->pobierz_wspolrzedne();
                    WektorXY wsp_b = (*j)->pobierz_wspolrzedne();
                    std::pair<float, float> wym_band = (*j)->pobierz_wymiary().pobierz_wspolrzedne();
                    if (wym_band.first == 0){
                        predkosc_k.oblicz_kat(WektorXY(std::pair<float, float>{10, 0}));
                        (*i)->ustaw_predkosc(predkosc_k);
                    }
                    else if (wym_band.second == 0){
                        predkosc_k.oblicz_kat(WektorXY(std::pair<float, float>{0, 10}));
                        (*i)->ustaw_predkosc(predkosc_k);
                    }
                }

            }
        }

    WektorXY n;
    WektorXY w;
    for(std::vector<CObiekt*>::iterator i = obiekty.begin(); i!=obiekty.end(); ++i)
    {
        n = (*i)-> pobierz_predkosc();
        w = (*i) -> pobierz_wspolrzedne();
        (*i)->ustaw_wspolrzedne(n+w);
    }

    this->powiadamiaj_obserwatorow();
}
