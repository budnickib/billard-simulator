#include "Aplikacja.h"

Aplikacja::Aplikacja()
{
    stol = new CStol(1000, 600);
    grafika = new Grafika(stol);
    timer = new Timer(std::chrono::milliseconds(10));
    stol->dodaj_obserwatora(grafika);
    stol->dodaj_kule(50, WektorXY(std::pair<float, float>{100, 100}), WektorXY(std::pair<float, float>{0.5, 0.5 }));
    stol->dodaj_kule(20, WektorXY(std::pair<float, float>{100, 300}), WektorXY(std::pair<float, float>{1, 1}));
    stol->dodaj_kule(30, WektorXY(std::pair<float, float>{500, 300}), WektorXY(std::pair<float, float>{1, -1}));
    stol->dodaj_bande(WektorXY(std::pair<float, float>{10, 20}), WektorXY(std::pair<float, float>{0, 550}));
    stol->dodaj_bande(WektorXY(std::pair<float, float>{20, 10}), WektorXY(std::pair<float, float>{950, 0}));
    stol->dodaj_bande(WektorXY(std::pair<float, float>{20, 580}), WektorXY(std::pair<float, float>{950, 0}));
    stol->dodaj_bande(WektorXY(std::pair<float, float>{960, 20}), WektorXY(std::pair<float, float>{0, 550}));
}

Aplikacja::~Aplikacja()
{
    delete stol;
    delete grafika;
    delete timer;
}

void Aplikacja::dzialaj()
{
    while(grafika->okno.isOpen()){
        while (grafika->okno.pollEvent(grafika->zdarzenie))
        {
            if (grafika->zdarzenie.type == sf::Event::Closed)
                grafika->okno.close();
        }
        if (timer->pick())
            stol->poruszaj();
    }
}
