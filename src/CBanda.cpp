#include "CBanda.h"

CBanda::CBanda(const WektorXY& q, const WektorXY& w):
    wymiary(w)
{
    wspolrzedne = q;
    predkosc = WektorXY(std::pair<float, float>{0, 0});
}

CBanda::~CBanda()
{
    //dtor
}

void CBanda::ustaw_wspolrzedne(const WektorXY& q)
{
    wspolrzedne = q;
}
