#include "Timer.h"
#include <chrono>
#include <iostream>

Timer::Timer(std::chrono::milliseconds q) : nastawa(q)
{
    uruchomienie = std::chrono::system_clock::now();
    sygnal = false;
}

Timer::~Timer()
{
    //dtor
}

bool Timer::pick()
{
    //sygnal = false;
    if (uruchomienie + nastawa < std::chrono::system_clock::now())
    {
        uruchomienie = std::chrono::system_clock::now();
        //std::cout<<"timer"<<std::endl;
        return true;
    }
    return false;
}

void Timer::reset()
{
    sygnal = false;
}
