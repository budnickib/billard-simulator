#ifndef WEKTORXY_H
#define WEKTORXY_H
#include <vector>

class WektorXY
{
    public:
        WektorXY(std::pair<float, float>);
        WektorXY(float d = 2.0f, float f = 0.78539816f);
        virtual ~WektorXY();

        void ustaw_wspolrzedne (const std::pair<float, float>&);
        void ustaw_dlugosc (const float&);
        void ustaw_fi (const float&);

        inline std::pair<float, float> pobierz_wspolrzedne() const;
        inline float pobierz_dlugosc() const {return dlugosc;}
        float pobierz_fi() const;
        void oblicz_kat(const WektorXY& dana);
        WektorXY& orto();

        WektorXY operator- ();
        inline WektorXY operator+ () const;
        WektorXY operator+ (const WektorXY&) const;
        WektorXY operator- (const WektorXY&) const;
    protected:

    private:
        std::pair<float, float> wspolrzedne;
        float dlugosc;
        float fi;
};

#endif // WEKTORXY_H
