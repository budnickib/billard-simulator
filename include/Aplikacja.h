#ifndef APLIKACJA_H
#define APLIKACJA_H
#include "CStol.h"
#include "Grafika.h"
#include "Timer.h"
#include <chrono>
#include <ratio>

class Aplikacja
{
    public:
        Aplikacja();
        virtual ~Aplikacja();

        void dzialaj();
    protected:

    private:
        CStol* stol;
        Grafika* grafika;
        Timer* timer;
};

#endif // APLIKACJA_H
