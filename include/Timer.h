#ifndef TIMER_H
#define TIMER_H
#include <chrono>

class Timer
{
    public:
        Timer(std::chrono::milliseconds);
        virtual ~Timer();
        bool pick();

    protected:
        std::chrono::milliseconds nastawa;
        void reset();
    private:
        std::chrono::system_clock::time_point uruchomienie;
        bool sygnal;
};

#endif // TIMER_H
