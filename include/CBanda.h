#ifndef CBANDA_H
#define CBANDA_H
#include "CObiekt.h"

class CBanda : public CObiekt
{
    public:
//        CBanda(float q=20, float w=30);
        CBanda(const WektorXY&, const WektorXY&);
        virtual ~CBanda();

        inline WektorXY& pobierz_wymiary() {return wymiary;}

        void ustaw_wspolrzedne(const WektorXY&);
        void ustaw_wymiary(float p, float s) {wymiary = {p, s};}
        void ustaw_predkosc(const WektorXY&) {}
    protected:

    private:
        WektorXY wymiary;

};

#endif // CBANDA_H
