#ifndef CSTOL_H
#define CSTOL_H
#include <vector>
#include "CObiekt.h"
#include "Analizator_Ruchu.h"
#include "Obserwowany.h"
#include "Obserwator.h"

class CStol : public Obserwowany
{
    public:
        CStol(int d=100, int s=200);
        virtual ~CStol();
        void dodaj_bande(const WektorXY&, const WektorXY&);
        void dodaj_kule(float, const WektorXY&, const WektorXY&);
        void powiadamiaj_obserwatorow();
        void dodaj_obserwatora(Obserwator*);
        void usun_obserwatora(Obserwator*);
        void poruszaj();

        std::vector<CObiekt*>& pobierz_obiekty() {return obiekty;}
    private:
        int dlugosc;
        int szerokosc;
        std::vector<CObiekt*> obiekty;
        Analizator_Ruchu* analiza;
};

#endif // CSTOL_H
