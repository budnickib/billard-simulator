#ifndef ANALIZATOR_RUCHU_H
#define ANALIZATOR_RUCHU_H
#include "CObiekt.h"
#include <vector>

class Analizator_Ruchu
{
    public:
        Analizator_Ruchu(std::vector<CObiekt*>&);
        virtual ~Analizator_Ruchu();

        bool sprawdz_obszar(CObiekt* const);//const WektorXY&, const std::pair<float, float>&) const
        bool sprawdz_kolizje(CObiekt* const, CObiekt* const);

    protected:

    private:
         std::vector<CObiekt*> tab;
};

#endif // ANALIZATOR_RUCHU_H
