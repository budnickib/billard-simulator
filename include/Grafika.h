#ifndef GRAFIKA_H
#define GRAFIKA_H
#include "Obserwator.h"
#include <SFML/Graphics.hpp>
#include "CStol.h"

class Grafika : public Obserwator//, public sf::RenderWindow
{
    public:
        Grafika(CStol*);
        virtual ~Grafika();

        sf::RenderWindow okno;
        sf::Texture tekstura_stolu;
        sf::Sprite tlo;
        std::vector<sf::CircleShape> kule;
        sf::Event zdarzenie;

        void update();
    protected:
        CStol* obiekt_obserwowany;
    private:
};

#endif // GRAFIKA_H
