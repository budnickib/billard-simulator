#ifndef CKULA_H
#define CKULA_H
#include <iostream>
#include "WektorXY.h"
#include "CObiekt.h"

class CKula : public CObiekt
{
    public:
        CKula(float p=10);
        CKula(float, const WektorXY&, const WektorXY&);
        virtual ~CKula() {}

        WektorXY& pobierz_wymiary();

        void ustaw_promien(const float&);
        void ustaw_predkosc(const WektorXY&);
        void ustaw_wspolrzedne(const WektorXY&);
    protected:
    private:
        float promien;
        WektorXY wymiary;
};
#endif // CKULA_H
