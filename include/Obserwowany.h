#ifndef OBSERWOWANY_H
#define OBSERWOWANY_H
#include "Obserwator.h"
#include <vector>

class Obserwowany
{
    public:
        Obserwowany();
        virtual ~Obserwowany();

        virtual void dodaj_obserwatora(Obserwator*)=0;
        virtual void usun_obserwatora(Obserwator*)=0;
        virtual void powiadamiaj_obserwatorow()=0;
    protected:
        std::vector<Obserwator*> obserwatorzy;
    private:
};

#endif // OBSERWOWANY_H
