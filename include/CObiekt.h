#ifndef COBIEKT_H
#define COBIEKT_H
#include "WektorXY.h"

class CObiekt
{
    public:
        CObiekt();
        virtual ~CObiekt();

        virtual void ustaw_wspolrzedne(const WektorXY&) = 0;
        virtual WektorXY& pobierz_wymiary()=0;
        virtual void ustaw_predkosc(const WektorXY&) = 0;
        inline WektorXY pobierz_wspolrzedne() const {return wspolrzedne;}
        inline static int pobierz_ilosc()  {return ilosc_obiektow-1;}
        inline int pobierz_numer() const {return numer_obiektu;}
        inline WektorXY pobierz_predkosc() const {return predkosc;}

    protected:
        WektorXY wspolrzedne;
        int numer_obiektu;
        static int ilosc_obiektow;
        WektorXY predkosc;
    private:
};

#endif // COBIEKT_H
